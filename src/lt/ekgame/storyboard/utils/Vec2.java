package lt.ekgame.storyboard.utils;

public class Vec2 {

    public static final Vec2 CENTER = new Vec2(320, 240);
    public static final Vec2 TOP_LEFT = new Vec2(0, 0);
    public static final Vec2 TOP_RIGHT = new Vec2(640, 0);
    public static final Vec2 BOTTOM_LEFT = new Vec2(0, 480);
    public static final Vec2 BOTTOM_RIGHT = new Vec2(640, 480);

    private double x, y;

    public Vec2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distance(Vec2 other) {
        return Math.sqrt(Math.pow(x - other.x, 2) + Math.pow(y - other.y, 2));
    }

    public Vec2 add(Vec2 other) {
        return new Vec2(x + other.x, y + other.y);
    }

    public Vec2 subtract(Vec2 other) {
        return new Vec2(x - other.x, y - other.y);
    }

    public Vec2 multiply(double scale) {
        return new Vec2(x*scale , y*scale);
    }

    public Vec2 divide(double scale) {
        return new Vec2(x/scale, y/scale);
    }

    public Vec2 normalize() {
        double length = Math.sqrt(x*x + y*y);
        return new Vec2(x/length, y/length);
    }
}
