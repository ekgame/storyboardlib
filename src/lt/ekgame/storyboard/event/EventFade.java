package lt.ekgame.storyboard.event;

import lt.ekgame.storyboard.utils.Utils;

import java.util.Locale;

public class EventFade extends SpriteEvent<Double> {

	public EventFade(long time, double alpha) {
		this(Easing.LINEAR, time, time, alpha, alpha);
	}

	public EventFade(long startTime, long endTime, double alphaFrom, double alphaTo) {
		this(Easing.LINEAR, startTime, endTime, alphaFrom, alphaTo);
	}

	EventFade(Easing easing, long startTime, long endTime, double alphaFrom, double alphaTo) {
		super(easing, startTime, endTime);
		fadeTo(alphaFrom);
		fadeTo(alphaTo);
	}

	public EventFade fadeTo(double alpha) {
		assert alpha >= 0 && alpha <= 1 : "Fading alpha can only be between 0 and 1 (inclusive).";
		parameters.add(alpha);
		return this;
	}

	@Override
	public String toDeclaration(int indent) {
		String line = Utils.indent(indent) + String.format(Locale.US, "F, %d, %d, %d",
				easing.getValue(), startTime, endTime);
		for (Double param : parameters)
			line += String.format(Locale.US, ", %s", Utils.formatDouble(param));
		return line;
	}
}
