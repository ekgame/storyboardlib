package lt.ekgame.storyboard.utils;

public enum Effect {

    FLIP_HORIZONTAL("H"), FLIP_VERTICAL("V"), ADDITIVE("A");

    private String value;

    Effect(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
