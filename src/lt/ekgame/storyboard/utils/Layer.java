package lt.ekgame.storyboard.utils;

public enum Layer {
	BACKGROUND("Background"), FAIL("Fail"), PASS("Pass"), FOREGROUND("Foreground");
	
	private String value;
	
	Layer(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
