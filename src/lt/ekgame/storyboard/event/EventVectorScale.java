package lt.ekgame.storyboard.event;

import lt.ekgame.storyboard.utils.Utils;
import lt.ekgame.storyboard.utils.Vec2;

import java.util.Locale;

public class EventVectorScale extends SpriteEvent<Vec2> {

    public EventVectorScale(long time, Vec2 scale) {
        this(Easing.LINEAR, time, time, scale);
    }

    public EventVectorScale(long startTime, long endTime, Vec2 scaleFrom, Vec2 scaleTo) {
        this(Easing.LINEAR, startTime, endTime, scaleFrom, scaleTo);
    }

    public EventVectorScale(long startTime, long endTime, Vec2 scale) {
        this(Easing.LINEAR, startTime, endTime, scale);
    }

    public EventVectorScale(Easing easing, long startTime, long endTime, Vec2 scaleFrom, Vec2 scaleTo) {
        super(easing, startTime, endTime);
        scaleTo(scaleFrom);
        scaleTo(scaleTo);
    }

    public EventVectorScale(Easing easing, long startTime, long endTime, Vec2 scale) {
        super(easing, startTime, endTime);
        scaleTo(scale);
    }

    public EventVectorScale scaleTo(Vec2 scale) {
        assert scale != null : "Scale can not be null.";
        parameters.add(scale);
        return this;
    }

    @Override
    public String toDeclaration(int indent) {
        String line = Utils.indent(indent) + String.format(Locale.US, "V, %d, %d, %d",
                easing.getValue(), startTime, endTime);
        for (Vec2 param : parameters)
            line += String.format(Locale.US, ", %s, %s",
                    Utils.formatDouble(param.getX()), Utils.formatDouble(param.getY()));
        return line;
    }
}
