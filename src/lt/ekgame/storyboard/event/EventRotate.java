package lt.ekgame.storyboard.event;

import lt.ekgame.storyboard.utils.Utils;

import java.util.Locale;

public class EventRotate extends SpriteEvent<Double> {

    public EventRotate(long time, double rotation) {
        this(Easing.LINEAR, time, time, rotation);
    }

    public EventRotate(long startTime, long endTime, double rotationStart, double rotationEnd) {
        this(Easing.LINEAR, startTime, endTime, rotationStart, rotationEnd);
    }

    public EventRotate(long startTime, long endTime, double rotation) {
        this(Easing.LINEAR, startTime, endTime, rotation);
    }

    public EventRotate(Easing easing, long startTime, long endTime, double rotationStart, double rotationEnd) {
        super(easing, startTime, endTime);
        rotateTo(rotationStart);
        rotateTo(rotationEnd);
    }

    public EventRotate(Easing easing, long startTime, long endTime, double rotation) {
        super(easing, startTime, endTime);
        rotateTo(rotation);
    }

    public EventRotate rotateTo(double rotation) {
        parameters.add(rotation);
        return this;
    }

    @Override
    public String toDeclaration(int indent) {
        String line = Utils.indent(indent) + String.format(Locale.US, "R, %d, %d, %d",
                easing.getValue(), startTime, endTime);
        for (Double param : parameters)
            line += String.format(Locale.US, ", %s", Utils.formatDouble(param));
        return line;
    }
}
