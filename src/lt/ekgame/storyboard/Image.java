package lt.ekgame.storyboard;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Image {

    private String relativePath;
    private Storyboard storyboard;
    private int width, height;

    public Image(String path, Storyboard storyboard) throws IOException {
        assert path != null : "Path can not be null.";
        assert storyboard != null : "Storyboard can not be null.";

        this.relativePath = path;
        this.storyboard = storyboard;

        BufferedImage img = ImageIO.read(new File(storyboard.getBeatmapFolder(), path));
        width = img.getWidth();
        height = img.getHeight();
    }

    public Storyboard getStoryboard() {
        return storyboard;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getPath() {
        return relativePath;
    }

}
