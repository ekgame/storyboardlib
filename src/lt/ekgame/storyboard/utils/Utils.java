package lt.ekgame.storyboard.utils;

import java.util.Locale;

public class Utils {
	
	public static String indent(int indent) {
		return new String(new char[indent]).replace("\0", " ");
	}

	public static double clamp(double value, double min, double max) {
		return value < min ? min : value > max ? max : value;
	}

	public static String formatDouble(double number) {
		if (number == (int) number)
			return String.format(Locale.US, "%d", (int) number);
		else
			return String.format(Locale.US, "%.4f", number);
	}

}
