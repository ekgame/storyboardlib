package lt.ekgame.storyboard.event;

import lt.ekgame.storyboard.utils.Color;
import lt.ekgame.storyboard.utils.Utils;

import java.util.Locale;

public class EventColor extends SpriteEvent<Color> {

    public EventColor(long time, Color color) {
        this(Easing.LINEAR, time, time, color);
    }

    public EventColor(long startTime, long endTime, Color colorFrom, Color colorTo) {
        this(Easing.LINEAR, startTime, endTime, colorFrom, colorTo);
    }

    public EventColor(long startTime, long endTime, Color color) {
        this(Easing.LINEAR, startTime, endTime, color);
    }

    public EventColor(Easing easing, long startTime, long endTime, Color colorFrom, Color colorTo) {
        super(easing, startTime, endTime);
        colorTo(colorFrom);
        colorTo(colorTo);
    }

    public EventColor(Easing easing, long startTime, long endTime, Color color) {
        super(easing, startTime, endTime);
        colorTo(color);
    }

    public EventColor colorTo(Color color) {
        assert color != null : "Color can not be null";
        parameters.add(color);
        return this;
    }

    @Override
    public String toDeclaration(int indent) {
        String line = Utils.indent(indent) + String.format(Locale.US, "V, %d, %d, %d",
                easing.getValue(), startTime, endTime);
        for (Color param : parameters)
            line += String.format(Locale.US, ", %d, %d, %d",
                    param.getRed(), param.getGreen(), param.getBlue());
        return line;
    }
}
