package lt.ekgame.storyboard;

import lt.ekgame.storyboard.event.EventRegistry;
import lt.ekgame.storyboard.utils.*;
import lt.ekgame.storyboard.utils.Layer;

public abstract class StoryboardObject extends EventRegistry {
	
	protected lt.ekgame.storyboard.utils.Layer layer;
	protected lt.ekgame.storyboard.utils.Origin origin;
	protected Image image;
	protected Vec2 position;
	
	public StoryboardObject(Image image, Vec2 position) {
		this(lt.ekgame.storyboard.utils.Origin.TOP_LEFT, image, position);
	}

	public StoryboardObject(lt.ekgame.storyboard.utils.Origin origin, Image image, Vec2 position) {
		this(lt.ekgame.storyboard.utils.Layer.BACKGROUND, origin, image, position);
	}
	
	public StoryboardObject(lt.ekgame.storyboard.utils.Layer layer, lt.ekgame.storyboard.utils.Origin origin, Image image, Vec2 position) {
		assert layer != null : "Layer can not be null";
		assert origin != null : "Origin can not be null";
		assert image != null : "Image can not be null";
		assert position != null : "Position can not be null";

		this.layer = layer;
		this.origin = origin;
		this.image = image;
		this.position = position;
	}
	
	public Layer getLayer() {
		return layer;
	}

	public lt.ekgame.storyboard.utils.Origin getOrigin() {
		return origin;
	}
	
	public Image getImage() {
		return image;
	}

	public Vec2 getPosition() {
		return position;
	}

	public abstract String getDeclaration();

}
