package lt.ekgame.storyboard.event;

import lt.ekgame.storyboard.utils.Effect;
import lt.ekgame.storyboard.utils.Utils;

import java.util.Locale;

public class EventEffect extends Event {

    private Effect effect;

    public EventEffect(Effect effect) {
        this(0, 0, effect);
    }

    public EventEffect(long startTime, long endTime, Effect effect) {
        super(startTime, endTime);
        assert effect != null : "Effect can not be null";
        this.effect = effect;
    }

    public Effect getEffect() {
        return effect;
    }

    @Override
    public String toDeclaration(int indent) {
        return Utils.indent(indent) + String.format(Locale.US, "V, 0, %d, %d, %s",
                startTime, endTime, effect.getValue());
    }
}
