package lt.ekgame.storyboard.event;

import lt.ekgame.storyboard.utils.Utils;

import java.util.Locale;

public class EventMoveAxis extends SpriteEvent<Integer> {

    private Axis axis;

    public EventMoveAxis(long time, Axis axis, int pos) {
        this(Easing.LINEAR, time, time, axis, pos);
    }

    public EventMoveAxis(long startTime, long endTime, Axis axis, int pos) {
        this(Easing.LINEAR, startTime, endTime, axis, pos);
    }

    public EventMoveAxis(long startTime, long endTime, Axis axis, int startPos, int endPos) {
        this(Easing.LINEAR, startTime, endTime, axis, startPos, endPos);
    }

    public EventMoveAxis(Easing easing, long startTime, long endTime, Axis axis, int startPos, int endPos) {
        super(easing, startTime, endTime);
        assert axis != null : "Axis can not be null.";

        moveTo(startPos);
        moveTo(endPos);
        this.axis = axis;
    }

    public EventMoveAxis(Easing easing, long startTime, long endTime, Axis axis, int pos) {
        super(easing, startTime, endTime);
        assert axis != null : "Axis can not be null.";

        moveTo(pos);
        this.axis = axis;
    }

    public Axis getAxis() {
        return axis;
    }

    public EventMoveAxis moveTo(int position) {
        parameters.add(position);
        return this;
    }

    @Override
    public String toDeclaration(int indent) {
        String line = Utils.indent(indent) + String.format(Locale.US, "M%s, %d, %d, %d",
                axis == Axis.X ? "X" : "Y", easing.getValue(), startTime, endTime);
        for (Integer param : parameters)
            line += String.format(Locale.US, ", %d", param);
        return line;
    }

    public enum Axis {
        X, Y
    }
}
