package lt.ekgame.storyboard.event;

import lt.ekgame.storyboard.utils.Pair;
import lt.ekgame.storyboard.utils.Vec2;
import lt.ekgame.storyboard.utils.Utils;

import java.util.Locale;

public class EventMove extends SpriteEvent<Vec2> {

    public EventMove(long time, Vec2 position) {
        this(time, time, position);
    }

    public EventMove(long startTime, long endTime, Vec2 position) {
        super(Easing.LINEAR, startTime, endTime);
        moveTo(position);
    }

    public EventMove(Easing easing, long startTime, long endTime, Vec2 from, Vec2 to) {
        super(easing, startTime, endTime);
        moveTo(from);
        moveTo(to);
    }

    public EventMove moveTo(Vec2 target) {
        assert target != null : "Target position may not be null";
        parameters.add(target);
        return this;
    }

    @Override
    public String toDeclaration(int indent) {
        String line = Utils.indent(indent) + String.format(Locale.US, "M, %d, %d, %d",
                easing.getValue(), startTime, endTime);
        for (Vec2 param : parameters)
            line += String.format(Locale.US, ", %d, %d", (int)param.getX(), (int)param.getY());
        return line;
    }
}
