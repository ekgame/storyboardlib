package lt.ekgame.storyboard;

import lt.ekgame.storyboard.event.Event;
import lt.ekgame.storyboard.utils.Layer;
import lt.ekgame.storyboard.utils.Origin;
import lt.ekgame.storyboard.utils.Vec2;

import java.io.*;
import java.util.*;

public class Storyboard {

    private List<StoryboardObject> objects = new ArrayList<>();
    private File beatmapFolder;
    private Map<String, Image> imageCache = new HashMap<>();

    public Storyboard(File beatmapFolder) {
        this.beatmapFolder = beatmapFolder;
    }

    public void addObject(StoryboardObject object) {
        objects.add(object);
    }

    public void removeObject(StoryboardObject object) {
        objects.remove(object);
    }

    public void compile(String fileName) throws IOException {
        PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter(new File(beatmapFolder, fileName))));
        output.println("[Events]");
        for (StoryboardObject obj : objects) {
            output.println(obj.getDeclaration());
            for (Event event : obj.getEvents())
                output.println(event.toDeclaration(1));
        }
        output.println("// End of file");
        // For some reason the editor throws a fit if you try to load a storyboard without comments.
        output.close();
    }

    public Sprite createSprite(String path, Vec2 position) throws IOException {
        return createSprite(path, Layer.BACKGROUND, Origin.CENTER, position);
    }

    public Sprite createSprite(String path, Origin origin, Vec2 position) throws IOException {
        return createSprite(path, Layer.BACKGROUND, origin, position);
    }

    public Sprite createSprite(String path, Layer layer, Origin origin, Vec2 position) throws IOException {
        Sprite sprite = new Sprite(layer, origin, getImage(path), position);
        objects.add(sprite);
        return sprite;
    }

    private Image getImage(String path) throws IOException {
        if (imageCache.containsKey(path))
            return imageCache.get(path);
        else {
            Image image = new Image(path, this);
            imageCache.put(path, image);
            return image;
        }
    }

    public File getBeatmapFolder() {
        return beatmapFolder;
    }
}
