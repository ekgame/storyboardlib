package lt.ekgame.storyboard.utils;

public enum Origin {
	
	TOP_LEFT("TopLeft"), TOP_CENTER("TopCentre"), TOP_RIGHT("TopRight"),
	CENTER_LEFT("CentreLeft"), CENTER("Centre"), CENTER_RIGHT("CentreRight"),
	BOTTOM_LEFT("BottomLeft"), BOTTOM_CENTER("BottomCentre"), BOTTOM_RIGHT("BottomRight");
	
	private String value;
	
	Origin(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
