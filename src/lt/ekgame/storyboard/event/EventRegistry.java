package lt.ekgame.storyboard.event;

import java.util.*;

public class EventRegistry {
	
	List<Event> events = new ArrayList<>();
	
	public void addEvent(Event event) {
		if (!events.contains(event))
			events.add(event);
	}
	
	public void removeEvent(Event event) {
		events.remove(event);
	}
	
	public List<Event> getEvents() {
		return Collections.unmodifiableList(events);
	}

	public Event getLastEvent() {
		return events.get(events.size() - 1);
	}

}
