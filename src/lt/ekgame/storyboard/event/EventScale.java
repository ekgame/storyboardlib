package lt.ekgame.storyboard.event;

import lt.ekgame.storyboard.utils.Utils;

import java.util.Locale;

public class EventScale extends SpriteEvent<Double> {

    public EventScale(long time, double scale) {
        this(Easing.LINEAR, time, time, scale);
    }

    public EventScale(long startTime, long endTime, double scale) {
        this(Easing.LINEAR, startTime, endTime, scale);
    }

    public EventScale(long startTime, long endTime, double scaleFrom, double scaleTo) {
        this(Easing.LINEAR, startTime, endTime, scaleFrom, scaleTo);
    }

    public EventScale(Easing easing, long startTime, long endTime, double scaleFrom, double scaleTo) {
        super(easing, startTime, endTime);
        scaleTo(scaleFrom);
        scaleTo(scaleTo);
    }

    public EventScale(Easing easing, long startTime, long endTime, double scale) {
        super(easing, startTime, endTime);
        scaleTo(scale);
    }

    public EventScale scaleTo(double scale) {
        assert scale >= 0 : "The scale can not be less than zero.";
        parameters.add(scale);
        return this;
    }

    @Override
    public String toDeclaration(int indent) {
        String line = Utils.indent(indent) + String.format(Locale.US, "S, %d, %d, %d",
                easing.getValue(), startTime, endTime);
        for (Double param : parameters)
            line += String.format(Locale.US, ", %s", Utils.formatDouble(param));
        return line;
    }
}
