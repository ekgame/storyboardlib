package lt.ekgame.storyboard.event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class SpriteEvent<P> extends Event {

	protected Easing easing;
	protected List<P> parameters = new ArrayList<>();
	
	SpriteEvent(Easing easing, long startTime, long endTime) {
		super(startTime, endTime);
		assert easing != null : "Easing can not be null";
		this.easing = easing;
	}
	
	public Easing getEasing() {
		return easing;
	}

	public List<P> getParameters() {
		return Collections.unmodifiableList(parameters);
	}

	public void removeParameter(P param) {
		parameters.remove(param);
	}

	/**
	 * Returns total time of this event, taking the number of parameter groups into account.
	 * @return long
	 */
	public long getTimeFrame() {
		return (endTime - startTime) * parameters.size();
	}

	/**
	 * Returns the point in time when the event finishes, taking the number of parameter groups into account.
	 * @return long
	 */
	public long getEndTimeStamp() {
		return startTime + getTimeFrame();
	}
}
