package lt.ekgame.storyboard.event;

/**
 * Visualizations available at http://easings.net/
 */
public enum Easing {

	LINEAR(0), EASE_OUT(1), EASE_IN(2),
	QUAD_IN(3), QUAD_OUT(4), QUAD_IN_OUT(5),
	CUBIC_IN(6), CUBIC_OUT(7), CUBIC_IN_OUT(8),
	QUART_IN(9), QUART_OUT(10), QUART_IN_OUT(11),
	QUINT_IN(12), QUINT_OUT(13), QUINT_IN_OUT(14),
	SINE_IN(15), SINE_OUT(16), SINE_IN_OUT(17),
	EXPO_IN(18), EXPO_OUT(19), EXPO_IN_OUT(20),
	CIRC_IN(21), CIRC_OUT(22), CIRC_IN_OUT(23),
	ELASTIC_IN(24), ELASTIC_OUT(25), ELASTIC_HALF_OUT(26),
	ELASTIC_QUARTER_OUT(27), ELASTIC_IN_OUT(28),
	BACK_IN(29), BACK_OUT(30), BACK_IN_OUT(31),
	BOUNCE_IN(32), BOUNCE_OUT(33), BOUNCE_IN_OUT(34);
	
	private int value;
	
	Easing(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
}