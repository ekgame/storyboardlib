package lt.ekgame.storyboard.event;

public abstract class Event {

    protected long startTime, endTime;

    public Event(long startTime, long endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public abstract String toDeclaration(int indent);
}
