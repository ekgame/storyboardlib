package lt.ekgame.storyboard;

import lt.ekgame.storyboard.event.Easing;
import lt.ekgame.storyboard.event.EventMove;
import lt.ekgame.storyboard.utils.Layer;
import lt.ekgame.storyboard.utils.Origin;
import lt.ekgame.storyboard.utils.Vec2;

public class Sprite extends StoryboardObject {

    public Sprite(Image image, Vec2 position) {
        super(Layer.BACKGROUND, Origin.CENTER, image, position);
    }

    public Sprite(Origin origin, Image image, Vec2 position) {
        super(Layer.BACKGROUND, origin, image, position);
    }

    public Sprite(Layer layer, Origin origin, Image image, Vec2 position) {
        super(layer, origin, image, position);
    }

    public void setStatic() {
        addEvent(new EventMove(Easing.LINEAR, 0, Integer.MAX_VALUE, position, position));
    }

    @Override
    public String getDeclaration() {
        return String.format("Sprite,%s,%s,\"%s\",%d,%d",
                layer.getValue(), origin.getValue(), image.getPath(), (int)position.getX(), (int)position.getY());
    }
}
